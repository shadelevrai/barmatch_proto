import React, { useState, useEffect } from "react";
import MapView, { Marker } from "react-native-maps";
import { firebase } from "../../firebaseConfig";
import { View, Button, Text } from "react-native";

export default function HomeScreen({ extraData, userChange }) {
  
  async function getCurrentPosition() {
    navigator.geolocation.getCurrentPosition((position) => {
      let region = {
        latitude: parseFloat(position.coords.latitude),
        longitude: parseFloat(position.coords.longitude),
        latitudeDelta: 5,
        longitudeDelta: 5,
      };
      setCurrentPosition(region);
    });
  }

  function fetchMarkerData() {
    fetch("https://feeds.citibikenyc.com/stations/stations.json")
      .then((response) => response.json())
      .then((responseJson) => {
        // this.setState({
        //   isLoading: false,
        //   markers: responseJson.stationBeanList,
        // });
        setIsLoading(false);
        setMarkers(responseJson.stationBeanList);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  useEffect(() => {
    fetchMarkerData();
  }, []);

  const [currentPosition, setCurrentPosition] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [markers, setMarkers] = useState([]);

  return (
    <>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        /*onRegionChange={()=>console.log("Je manipule la map")}*/
        showsUserLocation={true}
        onMapReady={() => getCurrentPosition()}
      >
        {isLoading
          ? null
          : markers.map((marker, index) => {
              const coords = {
                latitude: marker.latitude,
                longitude: marker.longitude,
              };
              const metadata = `Status: ${marker.statusValue}`;
              return (
                <MapView.Marker
                  key={index}
                  coordinate={coords}
                  title={marker.stationName}
                  description={metadata}
                />
              );
            })}
      </MapView>
      <Text>Hello {extraData.fullName}</Text>
      <Button
        title="Se déco"
        onPress={() =>
          firebase
            .auth()
            .signOut()
            .then(() => {
              userChange(null);
            })
        }
      >
        Se déco
      </Button>
    </>
  );
}
