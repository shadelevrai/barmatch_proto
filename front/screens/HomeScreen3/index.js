import React, { useState, useEffect, useCallback } from "react";
import { GoogleMap, useJsApiLoader } from "@react-google-maps/api";
import { firebase } from "../../firebaseConfig";
import { View, Button, Text } from "react-native";

export default function HomeScreen({ extraData, userChange }) {

  const containerStyle = {
    width: "375px",
    height: "500px",
  };

  const center = {
    lat: -3.745,
    lng: -38.523,
  };

  const { isLoaded } = useJsApiLoader({
    id: "Clé API 1",
    googleMapsApiKey: "AIzaSyA3Q6Pb0Sa9bEoKkTPY9wePUkslx6-0XmA",
  });

  const [map, setMap] = useState(null);
  const [currentPosition, setCurrentPosition] = useState(null);
  const [latitudeAndLongitude, setLatitudeAndLongitude] = useState({})

  const onLoad = useCallback(function callback(map) {
    navigator.geolocation.getCurrentPosition((position) => {
        let region = {
          latitude: parseFloat(position.coords.latitude),
          longitude: parseFloat(position.coords.longitude),
          latitudeDelta: 5,
          longitudeDelta: 5,
        };
        setCurrentPosition(region);
        setLatitudeAndLongitude({lat:region.latitude,lng:region.longitude})
      });

    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    setMap(map);
  }, []);

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);

  return isLoaded ? (
    <>
    {console.log(currentPosition)}
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={latitudeAndLongitude ? latitudeAndLongitude : center}
        zoom={10}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        {/* Child components, such as markers, info windows, etc. */}
        <></>
      </GoogleMap>
    </>
  ) : (
    <>
      <Text>nooop22pdsfspo</Text>
    </>
  );
}


