import React from "react"
import {View, Button} from "react-native"

export default function ProfileIcon({navigation}){
    return(
        <View>
            <Button title="Profil" onPress={()=>navigation.navigate("Profile")}>Profil</Button>
        </View>
    )
}   