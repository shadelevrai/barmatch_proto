import React, { useEffect, useState, useRef } from "react";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import { View, Text, Button } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { GOOGLE_API_KEY } from "@env";
import { firebase } from "../../firebaseConfig";
import _ from "lodash";

import ProfileIcon from "../../components/profileIcon";

const people = [
  {
    name: "le coucou cafe",
    position: {
      latitude: 48.88100632959281,
      longitude: 2.3427347280085087,
    },
    numberPeople: 15,
  },
  {
    name: "Beer pong bar",
    position: {
      latitude: 48.83737474813714,
      longitude: 2.360216695815325,
    },
    numberPeople: 10,
  },
  {
    name: "chez colette",
    position: {
      latitude: 48.861526791524724,
      longitude: 2.3777187801897526,
    },
    numberPeople: 50,
  },
];

export default function Map({ extraData, navigation }) {
  /**
   * @params {String} baseUrl
   */

  const [barPrevu, setBarPrevu] = useState();

  const [resultSearchBar, setResultSearchBar] = useState({
    name: null,
    location: {
      adress: null,
      lat: null,
      long: null,
      latDelta: null,
      lngDelta: null,
    },
    opening_hours: null,
  });

  const [positionUser, setPositionUser] = useState({
    coords: {
      accuracy: null,
      altitude: null,
      altitudeAccuracy: null,
      heading: null,
      latitude: 48.856614,
      longitude: 2.3522219,
      speed: null,
    },
    mocked: null,
    timestamp: null,
    predefinedPlaces: {
      description: "current location",
      geometry: {
        location: {
          formatted_address: null,
          lat: 48.856614,
          lng: 2.3522219,
        },
      },
    },
  });

  const [showAlertProposal, setShowAlertProposal] = useState(false);

  useEffect(() => {
    getCurrentLocation();
  }, []);

  const mapRef = useRef();

  function getCurrentLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      setPositionUser({
        coords: {
          accuracy: position.coords.accuracy,
          altitude: position.coords.altitude,
          altitudeAccuracy: position.coords.altitudeAccuracy,
          heading: position.coords.heading,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          speed: position.coords.speed,
        },
        mocked: position.mocked,
        timestamp: position.timestamp,
        predefinedPlaces: {
          description: "current location",
          geometry: {
            location: {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            },
          },
        },
      });
    });
  }

  // const barPrevuRef = ;
  const userID = extraData.id;

  function buttonAddPrevuBar() {
    // setBarPrevu(resultSearchBar.name);
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    const data = { barPrevu: resultSearchBar.name, createAt: timestamp };
    firebase
      .firestore()
      .collection("barPrevu")
      .doc(userID)
      .set(data)
      .then((_doc) => {
        console.log("ok");
      })
      .catch((error) => {
        alert(error);
      });
  }

  return (
    <>
      <ProfileIcon style={{ flex: 1 }} navigation={navigation} />
      <GooglePlacesAutocomplete
        style={{ flex: 1 }}
        nearbyPlacesAPI="paris"
        debounce={2000}
        fetchDetails={true}
        enablePoweredByContainer={false}
        currentLocation={false}
        placeholder="Search"
        onPress={(data, details) => {
          setResultSearchBar({
            name: details.name,
            location: {
              adress: details.formatted_address,
              lat: details.geometry.location.lat,
              lng: details.geometry.location.lng,
              latDelta: 0.0028812035741410114,
              lngDelta: 0.005274228751659393,
            },
            opening_hours: details.opening_hours,
          });
        }}
        query={{
          key: GOOGLE_API_KEY,
          language: "fr",
          components: "country:fr",
        }}
        predefinedPlaces={[positionUser.predefinedPlaces]}
      />
      <MapView
        ref={mapRef}
        showsMyLocationButton={true}
        onRegionChangeComplete={(region) => console.log(region)}
        provider={PROVIDER_GOOGLE}
        zoomEnabled={true}
        style={{ flex: 2 }}
        initialRegion={{
          latitude: 48.856614,
          longitude: 2.3522219,
          latitudeDelta: 0.1433501103269137,
          longitudeDelta: 0.14989960938692093,
        }}
        region={{
          latitude: resultSearchBar.location.lat
            ? resultSearchBar.location.lat
            : positionUser.coords.latitude,
          longitude: resultSearchBar.location.lng
            ? resultSearchBar.location.lng
            : positionUser.coords.longitude,
          latitudeDelta: resultSearchBar.location.latDelta
            ? resultSearchBar.location.latDelta
            : 0.0922,
          longitudeDelta: resultSearchBar.location.lngDelta
            ? resultSearchBar.location.lngDelta
            : 0.0421,
        }}
        showsUserLocation={true}
      >
        {resultSearchBar.name && (
          <>
            <MapView.Marker
              onPress={() => {
                console.log(
                  "Alerte pour montrer le nombre de personnes prévu ou déja sur place et lui proposer si elle veut aller à ce bar et à quelle heure"
                ),
                  setShowAlertProposal(true);
              }}
              coordinate={{
                latitude: resultSearchBar.location.lat,
                longitude: resultSearchBar.location.lng,
              }}
            >
              <MapView.Callout>
                <Text>{resultSearchBar.name}</Text>
                <Text>Nombre de personne : 5</Text>
              </MapView.Callout>
            </MapView.Marker>
          </>
        )}
        {people.map((item) => (
          <MapView.Marker
            key={item.name}
            coordinate={{
              latitude: item.position.latitude,
              longitude: item.position.longitude,
            }}
          >
            <Text>{item.numberPeople}</Text>
          </MapView.Marker>
        ))}
      </MapView>
      {showAlertProposal && (
        <>
          <View style={{ flex: 1 }}>
            <View>
              <Text onPress={() => console.log("oui je veux y aller")}>
                Voulez-vous y aller ?
              </Text>
            </View>
            <View>
              <Button title="oui" onPress={() => buttonAddPrevuBar()}></Button>
            </View>
          </View>
        </>
      )}
    </>
  );
}
