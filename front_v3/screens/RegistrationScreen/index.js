import React, { useEffect, useState } from "react";
import { firebase } from "../../firebaseConfig";
import DateTimePicker from "@react-native-community/datetimepicker";
// import {ImagePicker } from 'react-native-image-picker';
import * as ImagePicker from 'expo-image-picker';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  StyleSheet,
  Image
} from "react-native";
import {Picker} from "@react-native-picker/picker"

export default function RegistrationScreen({ navigation }) {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [dateBirth, setDateBirth] = useState();
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const [selectedValueGender, setSelectedValueGender] = useState("male");
  const [selectedValueGenderSearch, setSelectedValueGenderSearch] = useState()

  // const [resourcePath, setResourcePath] = useState({})
  const [image, setImage] = useState(null);

  //   useEffect(()=>{console.log(dateBirth)},[dateBirth])

  const onChangeDateBirth = (event, selectedDate) => {
    let eighteenYearsAgo = new Date();
    let eighteenYearsAgo1 = eighteenYearsAgo.setFullYear(
      eighteenYearsAgo.getFullYear() - 18
    );

    if (selectedDate.getTime() > eighteenYearsAgo1) {
      console.log('non, t"es mineur');
      const currentDate = selectedDate || date;
      setShow(Platform.OS === "ios");
      setDateBirth(currentDate);
    } else {
      console.log("ok t'es majeur");
      const currentDate = selectedDate || date;
      setShow(Platform.OS === "ios");
      setDateBirth(currentDate);
    }
  };

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  //   const showTimepicker = () => {
  //     showMode("time");
  //   };

  const onFooterLinkPress = () => {
    navigation.navigate("Login");
  };

  const onRegisterPress = () => {
    if (password !== confirmPassword) {
      alert("Passwords don't match.");
      return;
    }
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        const uid = response.user.uid;
        const data = {
          id: uid,
          email,
          fullName,
          years: 27,
          gender: selectedValueGender,
          genderSearch: selectedValueGenderSearch,
          dateBirth,
          photo:image
        };
        const usersRef = firebase.firestore().collection("users");
        usersRef
          .doc(uid)
          .set(data)
          .then(() => {
            navigation.navigate("Home", { user: data });
          })
          .catch((error) => {
            alert(error);
          });
      })
      .catch((error) => {
        alert(error);
      });
  };

  return (
    <>
      <KeyboardAwareScrollView>
        <TextInput
          placeholder="Full Name"
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setFullName(text)}
          value={fullName}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          placeholder="E-mail"
          placeholderTextColor="#aaaaaa"
          onChangeText={(text) => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          placeholder="Password"
          onChangeText={(text) => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          placeholder="Confirm Password"
          onChangeText={(text) => setConfirmPassword(text)}
          value={confirmPassword}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <TextInput type="date" />
        <TouchableOpacity onPress={() => onRegisterPress()}>
          <Text>Create account</Text>
        </TouchableOpacity>
        <View>
          <Text>Date de naissance</Text>
        </View>
        <View>
          <View>
            <Button onPress={showDatepicker} title="Show date picker!" />
          </View>
          {/* <View>
            <Button onPress={showTimepicker} title="Show time picker!" />
          </View> */}
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={new Date(897861600000)}
              mode={"date"}
              is24Hour={true}
              display="spinner"
              onChange={onChangeDateBirth}
            />
          )}
        </View>
        <View style={{ flex: 1, paddingTop: 40, alignItems: "center" }}>
            <Text>Je suis : </Text>
          <Picker
            selectedValue={selectedValueGender}
            style={{ height: 50, width: 150 }}
            onValueChange={(itemValue, itemIndex) =>
                setSelectedValueGender(itemValue)
            }
          >
            <Picker.Item label="Homme" value="male" />
            <Picker.Item label="Femme" value="female" />
            <Picker.Item label="Autre" value="other" />
          </Picker>
        </View>
        <View style={{ flex: 1, paddingTop: 40, alignItems: "center" }}>
            <Text>Je cherche : </Text>
          <Picker
            selectedValue={selectedValueGenderSearch}
            style={{ height: 50, width: 150 }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValueGenderSearch(itemValue)
            }
          >
            <Picker.Item label="Homme" value="male" />
            <Picker.Item label="Femme" value="female" />
            <Picker.Item label="Les deux" value="all" />
          </Picker>
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Pick an image from camera roll" onPress={pickImage} />
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
    </View>
        <View>
          <Text>
            Already got an account?{" "}
            <Text onPress={onFooterLinkPress}>Log in</Text>
          </Text>
        </View>
      </KeyboardAwareScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  button: {
    width: 250,
    height: 60,
    backgroundColor: '#3740ff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom:12    
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 15,
    color: '#fff'
  }
});