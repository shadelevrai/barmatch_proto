import React, { useEffect, useState } from "react";
import { firebase } from "../../firebaseConfig";
import { View, Button, Text, StyleSheet, SafeAreaView } from "react-native";
import storage from '@react-native-firebase/storage';
import * as ImagePicker from 'expo-image-picker';
import * as Progress from 'react-native-progress';

export default function ProfileScreen({ extraData, userChange }) {
  const [barPrevu, setBarPrevu] = useState(null);
  const userID = extraData.id;

  useEffect(() => {
    firebase
      .firestore()
      .collection("barPrevu")
      .doc(userID)
      .get()
      .then((result) => setBarPrevu(result.data()));
  }, []);

  useEffect(() => {
    console.log(barPrevu);
  });

  function cancelBar() {
    firebase
      .firestore()
      .collection("barPrevu")
      .doc(userID)
      .update({ barPrevu: null })
      .then(() => setBarPrevu(null));
  }

  function uploadUser() {
    firebase
      .firestore()
      .collection("users")
      .doc(userID)
      .update({ lala: "lala" })
      .then(() => console.log("upload user done"));
  }

  function reinitialiseMDP() {
    firebase
      .auth()
      .sendPasswordResetEmail(firebase.auth().currentUser.email)
      .then(() => console.log("email sent"))
      .catch((error) => console.log(error));
  }

  //https://github.com/ProProgramming101/expo-firebase-image-upload/blob/master/screens/HomeScreen.js
  //Voir la gestion d'erreur
  async function uploadImage(){
    let result = await ImagePicker.launchImageLibraryAsync();

    if (!result.cancelled) { 
        const response = await fetch(result.uri);
      const blob = await response.blob();  
      var ref = firebase.storage().ref().child(`${userID}/img`);
      return ref.put(blob);
    }
  }

  // async function uploadImage(uri){
  //   const response = await fetch(uri);
  //   console.log("🚀 ~ file: index.js ~ line 61 ~ uploadImage ~ response", response)
  //   const blob = await response.blob();  
  //   var ref = firebase.storage().ref().child("my-image");
  //   return ref.put(blob);
  // }

 

  useEffect(() => {
    // console.log(firebase.auth().currentUser)
  });

  return (
    <View>
      <View>
        <Text>bonjour {extraData.fullName}</Text>
      </View>
      <View>
        <Text>Vous êtes prévu pour ce bar : {barPrevu?.barPrevu} </Text>
      </View>
      <View>
        <Button
          title="Se déco"
          onPress={() =>
            firebase
              .auth()
              .signOut()
              .then(() => {
                userChange(null);
              })
          }
        >
          Se déco
        </Button>
      </View>
      <View>
        <Button title="Annuler le bar" onPress={cancelBar}></Button>
      </View>
      <View>
        <Button title="upload user" onPress={uploadUser}></Button>
      </View>
      <View>
        <Button title="reinitialiser son mdp" onPress={reinitialiseMDP}></Button>
      </View>
      <View>
        <Button title="Pick an image from camera roll" onPress={uploadImage} />
      </View>
    </View>
  );
}