import React, { Component } from "react";
import { Container, Content } from "native-base";
import { StyleSheet, Text, View } from 'react-native';

//Components
import SearchBar from "../SearchBar/SearchBar";
import MenuItem from "../Menu/Menu";

//Styles
import styles from "./styles";
class Dashboard extends Component {
  static navigationOptions = {
    headerTitle: "Find all Around Me!"
  };
  render() {
    const { navigation } = this.props;
    return (
      <Container style={styles.container}>
           <View>
      <Text>
      Dashboard
      </Text>
    </View>
         <SearchBar />
        <Content>
          <MenuItem navigation={navigation} />
        </Content> 
      </Container>
    );
  }
}

export default Dashboard;