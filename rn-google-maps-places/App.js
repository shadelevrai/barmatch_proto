//https://medium.com/better-programming/google-maps-and-places-in-a-real-world-react-native-app-100eff7474c6

import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import Dashboard from "./components/Dashboard/Dashboard"
import Navigator from "./components/Navigation/Navigator"; 

export default function App() {
  return (
    <>
     <Navigator/>
    </>
  );
}